xit('fetch buzz and expand', function() {
  element(':button:contains(fetch)').click();
  expect(repeater('div.buzz').count()).toBeGreaterThan(0);
  element('.buzz a:contains(Expand replies):first').click();
  expect(repeater('div.reply').count()).toBeGreaterThan(0);
});
