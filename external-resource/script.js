BuzzController.$inject = ['$scope', '$resource'];
function BuzzController($scope, $resource) {
 $scope.userId = 'googlebuzz';
 $scope.Activity = $resource(
  'https://www.googleapis.com/buzz/v1/activities/:userId/:visibility/:activityId/:comments',
  {alt: 'json', callback: 'JSON_CALLBACK'},
  { get:     {method: 'JSONP', params: {visibility: '@self'}},
    replies: {method: 'JSONP', params: {visibility: '@self', comments: '@comments'}}
  });
 
 $scope.fetch = function() {
  $scope.activities = $scope.Activity.get({userId:this.userId});
 }
 
 $scope.expandReplies = function(activity) {
  activity.replies = $scope.Activity.replies({userId: this.userId, activityId: activity.id});
 }
};
