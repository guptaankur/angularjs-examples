it('should navigate to URL', function () {
    element('a:contains(Welcome)').click();
    expect(element('[ng-view]').text()).toMatch(/Hello anonymous/);
    element('a:contains(Settings)').click();
    input('form.name').enter('yourname');
    element(':button:contains(Save)').click();
    element('a:contains(Welcome)').click();
    expect(element('[ng-view]').text()).toMatch(/Hello yourname/);
});
